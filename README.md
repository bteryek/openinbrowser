# OpenInBrowser
##

On save opens up a user defined URL in your default web browser.

##  Introduction

There are several plugins out there that will refresh or open up a file in the browser. However, they are usually limited to opening open the file you are working on. 

With OpenInBrowser, when you save a file, we scan your configuration settings and look for a match against the current file. If the file you saved matches the pattern you specified, OpenInBrowser will open up the associated url in your default web browser.

## Configuration

The configuration file is an array of objects. Each object has a single key, which is the pattern to test the saved file against. The value is the associated url you want to open if the file is matched.

Each object is checked in the order they appear in the array. Once it finds a match, it will execute and open up the associated url.

	[
	
		{"index.html": "http://idevelopsolutions.com"},
		{"index.(html|php)": "http://idevelopsolutions.com"},
		{"index.(html|php)": "http://idevelopsolutions.com/index.$1"}
	
	]

**Simple Match**

	{"index.html": "http://idevelopsolutions.com"}

If you are working on a file that matches the "index.html" pattern, it will open up [http://idevelopsolutions.com](http://idevelopsolutions.com)

**Regex Match**

	{"index.(html|php)": "http://idevelopsolutions.com"}

Files that match "index.html" or "index.php" will open up [http://idevelopsolutions.com](http://idevelopsolutions.com)

**Advanced Regex Match**

If you want to use groups from the regex pattern in the url you wish to open, use **$n** syntax to backreference a group.

	{"index.(html|php)": "http://idevelopsolutions.com/index.$1"}

Files that match index.html or index.php will open up 

- [http://idevelopsolutions.com/index.html](http://idevelopsolutions.com/index.html) or 
- [http://idevelopsolutions.com/index.php](http://idevelopsolutions.com/index.php)

## Install From Package Control

1. Search for OpenInBrowser.

## Install Manually:

1. Create a folder in the packages directory called "OpenInBrowser"
2. Download the latest tag, unzip and place the files in the  "OpenInBrowser" directory.

## Video Tutorial

[http://www.youtube.com/watch?v=MCmIazjhq9A](http://www.youtube.com/watch?v=MCmIazjhq9A)

## Install Duplicate Tab Plugin for your Browser

It is highly recommended you install a duplicate tab plugin for your web browser if you do not want to keep opening up the same url(s) in new tabs.

I wrote a Chrome plugin that works with OpenInBrowser Plugin:

- [OpenInBrowser Chrome Plugin](https://chrome.google.com/webstore/detail/openinbrowser/ddjnoikeiiiialgjgmanaienongjhamn)
- [Firefox](https://addons.mozilla.org/en-US/firefox/addon/duplicate-tab-closer/?src=api)

Check out my other plugin:

- [PhpCodeGen](https://bitbucket.org/bteryek/phpcodegen)