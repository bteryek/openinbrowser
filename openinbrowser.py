# https://bitbucket.org/bteryek/openinbrowser
# On save opens up a user defined URL

import sublime, sublime_plugin, re, webbrowser, json

class OpenInBrowser(sublime_plugin.EventListener):
	
	# location of the configuration file
	configFile = None

	# dict of settings from the openinbrowser.sublime-settings
	settings = None

	# saved file name
	savedFileName = None

	# url list
	urls = []

	# list of files with the path prepended to each index
	files = []

	# match object when search for a regex pattern match in a file
	match = None

	def on_post_save(self, view):

		self.urls = []
		self.files = []

		self.configFile = sublime.packages_path()+'/User/openinbrowser.sublime-settings'
		self.configFile = self.standardizePath(self.configFile)
		self.savedFileName = view.file_name()

		try:
			# open json config file and parse it as json
			f = open(self.configFile, 'r')
			data = f.read()
			f.close()
		except:
			print('Unable to open file: '+self.configFile)

		try:
			self.parseJSON(data)
		except:
			print('An error occurred trying to parse the json in the config file: ', self.configFile)

		blnResult = self.openUrl(self.savedFileName)
		if not blnResult:
			self.log('No match found!')

	def openUrl(self, fileName):

		# replace \ with / for windows compatibility
		fileName = self.standardizePath(fileName.lower())

		# loop through self.files and see if there is a match

		i = 0
		for fn in self.files:

			self.match = re.search(fn, fileName)
			if self.match != None:

				url = re.sub('(\$[0-9])', self.replacement, self.urls[i])
				webbrowser.open(url)
				self.log('Opening '+url)
				return True
				break

			i = i + 1

		return False

	def replacement(self, matchObject):

		groupNumber = int(matchObject.group()[1:])
		return self.match.group(groupNumber)

	def parseJSON(self, jsonStr):

		# remove all comments in the file, but not if preceeded by http:
		data = re.sub('[^http:]\/\/.*\n', '', jsonStr)
		
		# remove all tabs and newlines from string
		data = re.sub('\t|\n', '', data)

		data = json.dumps(data)
		config = json.loads(data)
		config = json.loads(config)

		# loop through files list and prepend the keys with the path

		for setting in config:
			for k,v in setting.items():
				self.files.append(k)
				self.urls.append(v)
				 
		# print self.files

	def standardizePath(self, path):

		# replace \ with / for windows compatibility
		return path.replace('\\', '/')

	def log(self, msg):
		print(msg)
